vim.g.mapleader = ","

vim.keymap.set("i", "jk", "<esc>", {noremap = true})

vim.keymap.set("n", "j", "gj", {noremap = true})
vim.keymap.set("n", "k", "gk", {noremap = true})

vim.keymap.set("n", "'", "`", {noremap = true})
vim.keymap.set("n", "`", "'", {noremap = true})
vim.keymap.set("n", "0", "^", {noremap = true})
vim.keymap.set("n", "^", "0", {noremap = true})

vim.keymap.set("n", "<c-j>", "o<esc>", {noremap = true})
vim.keymap.set("n", "<c-k>", "O<esc>", {noremap = true})
vim.keymap.set("n", "<c-h>", "hx<esc>", {noremap = true})
vim.keymap.set("n", "<c-l>", "i<space><esc>l", {noremap = true})

-- splitline
vim.keymap.set('n', 'S',
    function ()
        return [[:keeppatterns substitute/\s*\%#\s*/\r/e <bar> normal! ==k$<CR>]]
    end,
    { expr = true })

-- apply macro over visual range
vim.keymap.set('x', '@',
    function ()
        return ':norm @' .. vim.fn.getcharstr() .. '<cr>'
    end,
    { expr = true })
