M = {}

M.isort = {
    formatCommand = "isort --stdout ${-l:lineLength} --profile black -",
    formatStdin = true,
}

M.black = {
    formatCommand = "black -",
    formatStdin = true,
}

return M
