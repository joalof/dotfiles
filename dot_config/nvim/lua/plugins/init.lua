return {
    'tpope/vim-fugitive',
    'tpope/vim-repeat',
    'nvim-zh/better-escape.vim',
    'romainl/vim-cool',
    'romainl/vim-qf',
    'skywind3000/asyncrun.vim',
    {'JoshMcguigan/estream',  run = 'bash install.sh v0.2.0'},
}
