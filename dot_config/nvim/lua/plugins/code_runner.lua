require('code_runner').setup {
    mode = 'tab',
    focus = false,
    filetype = {
        python = "python3 -u",
    },
}

-- vim.keymap.set('n', '<leader>rr', ':RunCode<CR>', { noremap = true, silent = false })
