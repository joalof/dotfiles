vim.g.splitjoin_split_mapping = ''
vim.g.splitjoin_join_mapping = ''
vim.keymap.set('n', 'J', ':SplitjoinJoin<cr>')
vim.keymap.set('n', 'S', ':SplitjoinSplit<cr>')
