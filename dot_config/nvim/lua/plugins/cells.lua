-- get comment string for 
-- vim.bo.commentstring (grab relevant part)
-- or 
-- ft = require('Comment.ft')
-- ft.get(vim.bo.filetype) 
--
-- vim.pretty_print(MiniAi.gen_spec.pair('# %%', '# %%'))
-- gives genspec {"# %%%%().-()# %%%%%"}
-- add specs that match delmin + BOF / EOF?
--
local cellspec = function(mode)
    local re_delim = [[\M]] .. '#' .. [[\m]] .. '%%'
    local pos = api.nvim_win_get_cursor(0)
    
    -- move cursor to starting position for backward search
    api.nvim_win_set_cursor(0, {pos[1], vim.fn.col('$')})

    -- first delim or BOF
    local res = vim.fn.search(re_delim, 'ncWb', 1)
    local from_line = res == 0 and 0 or res + 1
 
    -- second delim or EOF
    local nlines = vim.api.nvim_buf_line_count(0)
    res = vim.fn.search(re_delim, 'nW', nlines)
    local to_line = res == 0 and nlines or res - 1

    -- define region
    local from = {line = from_line, col = 0}
    local to = {line = to_line, col = 0}

    api.nvim_win_set_cursor(0, pos)

    return {from = from, to = to}
end

-- terminator delimiter search
-- let delimiter = '\M' . l . '\m' . s:terminator_repl_delimiter_regex . '\M' . r
-- let save_pos = getpos(".")
--
-- " Find delimiters: always start from search_pos := end of current line.
-- " Last delimiter: search backwards (accept match at starting cursor pos).
-- let search_pos = [save_pos[0], save_pos[1], col('$'), save_pos[3]]
-- call setpos('.', search_pos)
-- let last_delim = search(delimiter, 'ncWb', line("0"))
-- if last_delim == 0
--     let ladjust = 0
-- else
--     let ladjust = 1
-- endif
--
-- " Next delimiter: search forwards (dont accept match at starting cursor pos).
-- let next_delim = search(delimiter, 'nW', line("$"))
-- if next_delim == 0
--     let next_delim = line('$')
--     let nadjust = 0
-- else
--     let nadjust = 1
-- endif
--
-- call setpos('.', save_pos)
--
-- let cell = getline(last_delim + ladjust, next_delim - nadjust)
-- let cell = filter(cell, '!empty(v:val)')
