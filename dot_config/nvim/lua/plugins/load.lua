-- bootstrap packer
local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup({
    function(use)
        use 'wbthomason/packer.nvim'

        -- treesitter
        use {
            'nvim-treesitter/nvim-treesitter',
            run = function() require('nvim-treesitter.install').update({with_sync = true}) end,
        }
        use 'nvim-treesitter/playground'
        use 'nvim-treesitter/nvim-treesitter-textobjects'

        use 'tpope/vim-fugitive'
        use 'tpope/vim-repeat'
        use 'numToStr/Comment.nvim'
        use 'kylechui/nvim-surround'
        use 'echasnovski/mini.ai'
        use 'nvim-zh/better-escape.vim'

        use 'linty-org/readline.nvim'
        use 'monaqa/dial.nvim'
        use 'romainl/vim-cool'
        use 'romainl/vim-qf'
        use 'alexghergh/nvim-tmux-navigation'
        use {
            'joalof/vim-kitty-navigator',
            run = 'cp ./*.py ~/.config/kitty/',
            branch = 'fix-multikey-mappings'
        }

        use 'nvim-lua/plenary.nvim'
        use 'ggandor/leap.nvim'
        use 'ggandor/flit.nvim'

        -- ui, colors
        use 'folke/tokyonight.nvim'
        use 'lukas-reineke/indent-blankline.nvim'
        use 'rcarriga/nvim-notify'

        -- running code, testing
        use 'skywind3000/asyncrun.vim'
        use {'JoshMcguigan/estream',  run = 'bash install.sh v0.2.0'}
        use {'vim-test/vim-test'}
        use {'erietz/vim-terminator', branch = 'main'}
        use {'dccsillag/magma-nvim', run = ':UpdateRemotePlugins'}
        -- use {'michaelb/sniprun', run = 'bash ./install.sh'}
        -- use {'CRAG666/code_runner.nvim', requires = 'nvim-lua/plenary.nvim'}

        -- lualine
        use {
            'nvim-lualine/lualine.nvim',
            requires = {'kyazdani42/nvim-web-devicons', opt = true}
        }

        -- lsp support
        use 'neovim/nvim-lspconfig'
        use 'williamboman/mason.nvim'
        use 'williamboman/mason-lspconfig.nvim'

        -- Autocompletion
        use 'hrsh7th/nvim-cmp'
        use 'hrsh7th/cmp-buffer'
        use 'hrsh7th/cmp-path'
        use 'saadparwaiz1/cmp_luasnip'
        use 'hrsh7th/cmp-nvim-lsp'
        use 'hrsh7th/cmp-nvim-lua'
        use 'onsails/lspkind.nvim'
        use 'ray-x/lsp_signature.nvim'
        -- use 'hrsh7th/cmp-nvim-lsp-signature-help'

        -- snippets
        use({"L3MON4D3/LuaSnip"})

        -- misc cmp/lsp related
        use "folke/neodev.nvim"

        -- fuzzy finding
        -- use {
        --     'nvim-telescope/telescope.nvim', tag = '0.1.0',
        --     requires = {'nvim-lua/plenary.nvim'}
        --     }
        use {
            'ibhagwan/fzf-lua',
            requires = {
                {'kyazdani42/nvim-web-devicons'},
                {'junegunn/fzf', run = './install --bin'}
            }
        }

        -- debugging
        use 'mfussenegger/nvim-dap'
        use {"rcarriga/nvim-dap-ui", requires = {"mfussenegger/nvim-dap"}}
        use 'HiPhish/debugpy.nvim'

        -- Automatically set up your configuration after cloning packer.nvim
        -- Put this at the end after all plugins
        if packer_bootstrap then
            require('packer').sync()
        end
  end,
  config = {
      display = {
          open_fn = function()
              return require('packer.util').float({ border = 'none'})
          end,
      }
  }
})
