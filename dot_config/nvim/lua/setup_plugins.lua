-- bootstrap packer
local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup({
    function(use)
        use 'wbthomason/packer.nvim'

        use 'tpope/vim-fugitive'
        use 'tpope/vim-unimpaired'
        use 'tpope/vim-repeat'
        use 'wellle/targets.vim'
        use 'kylechui/nvim-surround'
        use 'numToStr/Comment.nvim'

        -- colors
        use 'folke/tokyonight.nvim'

        -- treesitter
        use {
            'nvim-treesitter/nvim-treesitter',
            run = function() require('nvim-treesitter.install').update({with_sync = true}) end,
        }
        use 'nvim-treesitter/playground'

        -- lualine
        use {
            'nvim-lualine/lualine.nvim',
            requires = { 'kyazdani42/nvim-web-devicons', opt = true }
        }

        -- lsp support
        use 'neovim/nvim-lspconfig'
        use 'williamboman/mason.nvim'
        use 'williamboman/mason-lspconfig.nvim'

        -- Autocompletion
        use 'hrsh7th/nvim-cmp'
        use 'hrsh7th/cmp-buffer'
        use 'hrsh7th/cmp-path'
        use 'saadparwaiz1/cmp_luasnip'
        use 'hrsh7th/cmp-nvim-lsp'
        use 'hrsh7th/cmp-nvim-lua'
        use 'onsails/lspkind.nvim'

        -- snippets
        use  'L3MON4D3/LuaSnip'

        -- fuzzy finding
        -- use {
        --     'nvim-telescope/telescope.nvim', tag = '0.1.0',
        --     requires = {{'nvim-lua/plenary.nvim'}}
        --     }
        use {
            'ibhagwan/fzf-lua',
            requires = {
                {'kyazdani42/nvim-web-devicons'},
                {'junegunn/fzf', run = './install --bin'}
            }
        }

        use 'lukas-reineke/indent-blankline.nvim'
        use 'Vimjas/vim-python-pep8-indent'
        use 'alexghergh/nvim-tmux-navigation'

        -- Automatically set up your configuration after cloning packer.nvim
        -- Put this at the end after all plugins
        if packer_bootstrap then
            require('packer').sync()
        end
  end,
  config = {
      display = {
          open_fn = function()
              return require('packer.util').float({ border = 'none'})
          end,
      }
  }
})
