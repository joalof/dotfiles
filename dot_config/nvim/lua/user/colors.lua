require("tokyonight").setup({
    -- use the night style
    style = "moon",
    on_highlights = function(hl, c)
        hl.Constant = {fg = c.fg}
        hl.Number = {fg = c.yellow}
        hl.Boolean = {fg = c.yellow}
        hl.Float = {fg = c.yellow}
        hl.Comment = {fg = c.fg_gutter}
        hl["@variable.builtin"] = {fg = c.fg}
        hl["@parameter"] = {fg = c.fg}
    end
})

vim.cmd[[colorscheme tokyonight]]
