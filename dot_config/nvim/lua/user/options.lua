vim.opt.scrolloff = 3
vim.opt.sidescrolloff = 5
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.listchars = {
    eol = "$",
    tab = ">-",
    trail = "~",
    extends = ">",
    precedes = "<",
}

-- tabs
vim.opt.tabstop = 8
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.mousehide = false
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.number = true

-- performance
vim.opt.lazyredraw = true
vim.opt.history = 1000
vim.opt.synmaxcol = 500
vim.opt.tabpagemax = 50

-- searching
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- highlight match and yank
vim.opt.showmatch = true
vim.api.nvim_create_autocmd("TextYankPost", {
  callback = function()
    vim.highlight.on_yank()
  end,
})

-- autosaving
vim.opt.autowriteall = true
vim.api.nvim_create_autocmd("CursorHold", {
    pattern = "*",
    command = "update",
})

-- wildmenu
vim.opt.wildignore = {
    "*.o", "*.obj", "*~", "*.png", "*.jpg", "*.gif", "*.eps",
}
